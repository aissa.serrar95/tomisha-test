import 'package:flutter/material.dart';

const radiusValue = 12.0;

const color1 = Color(0xff319795);
const color2 = Color(0xff3182CE);
const color3 = Color(0xffEBF4FF);
const color4 = Color(0xffE6FFFA);
const textColor = Color(0xff2D3748);
const numberColor = Color(0xff718096);
const tabColor = Color(0xff81E6D9);
const borderColor = Color(0xffCBD5E0);

const linearGradient1 = LinearGradient(colors: [color1, color2]);
const linearGradient2 = LinearGradient(
  colors: [color4, color3],
  begin: Alignment.topLeft,
  end: Alignment.bottomCenter,
);
