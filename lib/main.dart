import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_wrapper.dart';
import 'package:tomish_test/mobile_layout/mobile_onboarding_screen.dart';
import 'package:tomish_test/web_layout/web_onboarding_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, child) => ResponsiveWrapper.builder(child,
          minWidth: 480,
          defaultScale: false,
          breakpoints: [
            // ResponsiveBreakpoint.resize(480, name: MOBILE),
            // ResponsiveBreakpoint.resize(800, name: TABLET),
            // ResponsiveBreakpoint.resize(1000, name: DESKTOP),
          ],
          background: Container(color: Color(0xFFF5F5F5))),
      title: 'Tomisha Test',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LayoutBuilder(
        builder: (p0, constraints) {
          return constraints.maxWidth < 600.0
              ? const MobileOnBoardingScreen()
              : const WebOnBoardingScreen();
        },
      ),
    );
  }
}
