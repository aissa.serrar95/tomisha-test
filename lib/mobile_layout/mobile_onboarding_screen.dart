import 'package:flutter/material.dart';
import 'package:tomish_test/constants.dart';
import 'package:tomish_test/widgets/custom_bnb.dart';
import 'package:tomish_test/widgets/custom_onborading_app_bar.dart';

class MobileOnBoardingScreen extends StatelessWidget {
  const MobileOnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomOnBoardingAppBar(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Section1(),
            const Section2(),
            const Section3(),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              height: MediaQuery.of(context).size.width,
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Positioned(
                    top: -64,
                    left: -32,
                    width: MediaQuery.of(context).size.width * .7,
                    height: MediaQuery.of(context).size.width * .7,
                    child: Container(
                      width: MediaQuery.of(context).size.width * .7,
                      decoration: const BoxDecoration(
                        color: Color(0xffF7FAFC),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.baseline,
                    // textBaseline: TextBaseline.alphabetic,
                    children: const [
                      Padding(
                        padding: EdgeInsets.only(left: 48),
                        child: Text(
                          "3.",
                          style: TextStyle(color: numberColor, fontSize: 130),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Mit nur einem Klick bewerben",
                          style: TextStyle(color: numberColor, fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    right: 32,
                    bottom: 64,
                    child: Image.asset("assets/undraw_personal_file_222m.png"),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: const CustomBNB(),
    );
  }
}

class Section3 extends StatelessWidget {
  const Section3({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * .5,
      child: ClipPath(
        clipper: Section3Clipper(),
        child: Container(
          decoration: BoxDecoration(gradient: linearGradient2),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.baseline,
                textBaseline: TextBaseline.alphabetic,
                children: const [
                  Padding(
                    padding: EdgeInsets.only(left: 48),
                    child: Text(
                      "2.",
                      style: TextStyle(color: numberColor, fontSize: 130),
                    ),
                  ),
                  Text(
                    "Erstellen dein Lebenslauf",
                    style: TextStyle(color: numberColor, fontSize: 16),
                  ),
                ],
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * .55,
                child: Image.asset(
                  "assets/undraw_task_31wc.png",
                  fit: BoxFit.fitWidth,
                ),
              ),
              SizedBox(height: 32)
            ],
          ),
        ),
      ),
    );
  }
}

class Section1 extends StatelessWidget {
  const Section1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * .75,
      child: ClipPath(
        clipBehavior: Clip.antiAlias,
        clipper: Section1Clipper(),
        child: Container(
          decoration: const BoxDecoration(
            gradient: linearGradient2,
          ),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.all(24.0),
                child: Text(
                  "Deine Job website",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 42,
                    letterSpacing: 1.26,
                    color: textColor,
                  ),
                ),
              ),
              Image.asset(
                "assets/undraw_agreement_aajr.png",
                fit: BoxFit.fitWidth,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Section2 extends StatefulWidget {
  const Section2({
    Key? key,
  }) : super(key: key);

  @override
  State<Section2> createState() => _Section2State();
}

class _Section2State extends State<Section2> with SingleTickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.height * .55,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 16),
          Center(
            child: Container(
              clipBehavior: Clip.antiAlias,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(radiusValue)),
              child: TabBar(
                controller: tabController,
                indicatorColor: tabColor,
                labelColor: Colors.white,
                unselectedLabelColor: color1,
                isScrollable: true,
                indicator: const BoxDecoration(color: tabColor),
                indicatorPadding: EdgeInsets.zero,
                tabs: [
                  Tab(
                    text: "Arbeitnehmer",
                  ),
                  Tab(
                    text: "Arbeitgeber",
                  ),
                  Tab(
                    text: "Temporärbüro",
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 16),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 64),
            child: SizedBox(
              width: size.width * .55,
              child: const Text(
                "Drei einfache Schritte zu deinem neuen Job",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: textColor,
                  fontSize: 21,
                ),
              ),
            ),
          ),
          const SizedBox(height: 16),
          Expanded(
            child: Stack(
              children: [
                Container(
                  width: size.width * .6,
                  decoration: const BoxDecoration(
                    color: Color(0xaaF7FAFC),
                    shape: BoxShape.circle,
                  ),
                ),
                Positioned(
                  right: 40,
                  child: SizedBox(
                    width: size.width * .6,
                    child: Image.asset(
                      "assets/undraw_Profile_data_re_v81r.png",
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 32,
                  left: 16,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: const [
                      Text(
                        "1.",
                        style: TextStyle(color: numberColor, fontSize: 130),
                      ),
                      Text(
                        "Erstellen dein Lebenslauf",
                        style: TextStyle(color: numberColor, fontSize: 16),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Section1Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path()
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height * .87)
      ..cubicTo(
        size.width * .7,
        size.height * .85,
        size.width * .3,
        size.height,
        0,
        size.height * .95,
      )
      ..close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}

class Section3Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path()
      ..cubicTo(
        size.width * .4,
        size.height * .2,
        size.width * .7,
        0,
        size.width,
        16,
      )
      ..lineTo(size.width, size.height * .9)
      ..cubicTo(
        size.width * .7,
        size.height * .85,
        size.width * .3,
        size.height,
        0,
        size.height * .98,
      )
      ..close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
