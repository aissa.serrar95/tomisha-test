import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomish_test/constants.dart';
import 'package:tomish_test/widgets/custom_onborading_app_bar.dart';
import 'package:tomish_test/widgets/gradient_button.dart';

class WebOnBoardingScreen extends StatefulWidget {
  const WebOnBoardingScreen({Key? key}) : super(key: key);

  @override
  State<WebOnBoardingScreen> createState() => _WebOnBoardingScreenState();
}

class _WebOnBoardingScreenState extends State<WebOnBoardingScreen> {
  bool showExtra = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWebOnBoardingAppBar(
        showExtra: showExtra,
      ),
      body: NotificationListener<ScrollNotification>(
        onNotification: (notification) {
          if ((notification.metrics as FixedScrollMetrics).pixels > 480.0) {
            if (!showExtra) {
              showExtra = true;
              setState(() {});
            }
          } else {
            if (showExtra) {
              showExtra = false;
              setState(() {});
            }
          }
          return true;
        },
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Section1(),
              const Section2(),
              const Section3(),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                height: 800,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      // crossAxisAlignment: CrossAxisAlignment.baseline,
                      // textBaseline: TextBaseline.alphabetic,
                      children: const [
                        Padding(
                          padding: EdgeInsets.only(left: 48),
                          child: Text(
                            "3.",
                            style: TextStyle(color: numberColor, fontSize: 130),
                          ),
                        ),
                        Text(
                          "Mit nur einem Klick\nbewerben",
                          style: TextStyle(color: numberColor, fontSize: 30),
                        ),
                      ],
                    ),
                    SizedBox(width: 32),
                    Image.asset(
                      "assets/undraw_personal_file_222m.png",
                      height: 400,
                      fit: BoxFit.fitHeight,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class Section3 extends StatelessWidget {
  const Section3({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 550,
      child: ClipPath(
        clipper: Section3Clipper(),
        child: Container(
          decoration: const BoxDecoration(gradient: linearGradient2),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/undraw_task_31wc.png",
                height: 300,
                fit: BoxFit.fitHeight,
              ),
              const SizedBox(width: 32),
              Row(
                crossAxisAlignment: CrossAxisAlignment.baseline,
                textBaseline: TextBaseline.alphabetic,
                children: const [
                  Padding(
                    padding: EdgeInsets.only(left: 48),
                    child: Text(
                      "2.",
                      style: TextStyle(color: numberColor, fontSize: 130),
                    ),
                  ),
                  Text(
                    "Erstellen dein Lebenslauf",
                    style: TextStyle(color: numberColor, fontSize: 30),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Section1 extends StatelessWidget {
  const Section1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 650,
      child: ClipPath(
        clipBehavior: Clip.antiAlias,
        clipper: Section1Clipper(),
        child: Container(
          decoration: const BoxDecoration(
            gradient: linearGradient2,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(flex: 2),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Deine Job website",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 60,
                        letterSpacing: 1.26,
                        color: textColor,
                      ),
                    ),
                    SizedBox(height: 64),
                    GradientButton()
                  ],
                ),
              ),
              Spacer(),
              Container(
                width: 450,
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
                child: Image.asset(
                  "assets/undraw_agreement_aajr.png",
                  fit: BoxFit.fitWidth,
                ),
              ),
              Spacer(flex: 2),
            ],
          ),
        ),
      ),
    );
  }
}

class Section2 extends StatefulWidget {
  const Section2({
    Key? key,
  }) : super(key: key);

  @override
  State<Section2> createState() => _Section2State();
}

class _Section2State extends State<Section2> with SingleTickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SizedBox(
      height: 600,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 16),
          Center(
            child: Container(
              clipBehavior: Clip.antiAlias,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(radiusValue)),
              child: TabBar(
                controller: tabController,
                indicatorColor: tabColor,
                labelColor: Colors.white,
                labelPadding: EdgeInsets.symmetric(horizontal: 48),
                unselectedLabelColor: color1,
                isScrollable: true,
                indicator: const BoxDecoration(color: tabColor),
                indicatorPadding: EdgeInsets.zero,
                tabs: [
                  Tab(
                    text: "Arbeitnehmer",
                  ),
                  Tab(
                    text: "Arbeitgeber",
                  ),
                  Tab(
                    text: "Temporärbüro",
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 16),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 64),
            child: SizedBox(
              width: size.width * .55,
              child: const Text(
                "Drei einfache Schritte\nzu deinem neuen Job",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: textColor,
                  fontSize: 34,
                ),
              ),
            ),
          ),
          const SizedBox(height: 16),
          Expanded(
            child: Stack(
              children: [
                // Positioned(
                //   top: 200,
                //   left: MediaQuery.of(context).size.width * .3,
                //   child: SvgPicture.asset("assets/1.svg"),
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      textBaseline: TextBaseline.alphabetic,
                      children: const [
                        Text(
                          "1.",
                          style: TextStyle(color: numberColor, fontSize: 130),
                        ),
                        Text(
                          "Erstellen dein Lebenslauf",
                          style: TextStyle(color: numberColor, fontSize: 30),
                        ),
                      ],
                    ),
                    SizedBox(width: 32),
                    SizedBox(
                      width: size.width * .2,
                      child: Image.asset(
                        "assets/undraw_Profile_data_re_v81r.png",
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Section1Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path()
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height * .87)
      ..cubicTo(
        size.width * .7,
        size.height * .85,
        size.width * .3,
        size.height,
        0,
        size.height * .95,
      )
      ..close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}

class Section3Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path()
      ..cubicTo(
        size.width * .4,
        size.height * .2,
        size.width * .7,
        0,
        size.width,
        16,
      )
      ..lineTo(size.width, size.height * .9)
      ..cubicTo(
        size.width * .7,
        size.height * .85,
        size.width * .3,
        size.height,
        0,
        size.height * .98,
      )
      ..close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
