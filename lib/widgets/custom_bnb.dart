import 'package:flutter/material.dart';
import 'package:tomish_test/constants.dart';
import 'package:tomish_test/widgets/gradient_button.dart';

class CustomBNB extends StatelessWidget {
  const CustomBNB({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 78 + MediaQuery.of(context).padding.bottom,
      child: BottomAppBar(
        color: Colors.transparent,
        elevation: 0,
        child: Container(
          clipBehavior: Clip.antiAlias,
          decoration: const BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                offset: Offset(0, -1),
                blurRadius: 3,
              ),
            ],
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(radiusValue),
              topRight: Radius.circular(radiusValue),
            ),
          ),
          padding: const EdgeInsets.all(16),
          child: const GradientButton(),
        ),
      ),
    );
  }
}
