import 'package:flutter/material.dart';
import 'package:tomish_test/constants.dart';
import 'package:tomish_test/widgets/gradient_button.dart';

class CustomOnBoardingAppBar extends StatelessWidget with PreferredSizeWidget {
  const CustomOnBoardingAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(radiusValue),
          bottomRight: Radius.circular(radiusValue),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            offset: Offset(0, 3),
            blurRadius: 6,
          ),
        ],
      ),
      child: SafeArea(
        child: Column(
          children: [
            Container(
              height: 5,
              decoration: const BoxDecoration(gradient: linearGradient1),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  const Text(
                    "Jetzt Klicken",
                    style: TextStyle(
                      color: Color(0xff4A5568),
                      fontSize: 19,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: CustomOutlinedButton(),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: TextButton(
                      onPressed: () {},
                      child: const Text(
                        "Login",
                        style: TextStyle(color: color1),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(80);
}

class CustomWebOnBoardingAppBar extends StatelessWidget with PreferredSizeWidget {
  const CustomWebOnBoardingAppBar({Key? key, this.showExtra = false}) : super(key: key);
  final bool showExtra;

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(radiusValue),
          bottomRight: Radius.circular(radiusValue),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            offset: Offset(0, 3),
            blurRadius: 6,
          ),
        ],
      ),
      child: SafeArea(
        child: Column(
          children: [
            Container(
              height: 5,
              decoration: const BoxDecoration(gradient: linearGradient1),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  showExtra
                      ? const Text(
                          "Jetzt Klicken",
                          style: TextStyle(
                            color: Color(0xff4A5568),
                            fontSize: 19,
                          ),
                        )
                      : const Center(),
                  showExtra
                      ? const Padding(
                          padding: EdgeInsets.all(16.0),
                          child: CustomOutlinedButton(),
                        )
                      : const Center(),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: TextButton(
                      onPressed: () {},
                      child: const Text(
                        "Login",
                        style: TextStyle(color: color1),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(80);
}
