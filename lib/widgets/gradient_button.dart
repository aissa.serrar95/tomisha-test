import 'package:flutter/material.dart';
import 'package:tomish_test/constants.dart';

class GradientButton extends StatelessWidget {
  const GradientButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: 40,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radiusValue),
      ),
      textColor: Colors.white,
      padding: EdgeInsets.zero,
      child: Ink(
        decoration: const BoxDecoration(gradient: linearGradient1),
        height: 48,
        width: double.infinity,
        child: Center(
          child: Text("Kostenlos Registrieren"),
        ),
      ),
      onPressed: () {},
    );
  }
}

class CustomOutlinedButton extends StatelessWidget {
  const CustomOutlinedButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: 40,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Color(0xffCBD5E0), width: 2),
        borderRadius: BorderRadius.circular(radiusValue),
      ),
      textColor: color1,
      padding: EdgeInsets.symmetric(horizontal: 64),
      child: Center(
        child: Text("Kostenlos Registrieren"),
      ),
      onPressed: () {},
    );
  }
}
